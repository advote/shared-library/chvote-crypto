/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-crypto                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.crypto.authentication;

/**
 * Public API for hashed an authentication information
 */
public interface AuthenticationHasher {

  /**
   * Generate a salt for hash method
   *
   * @return salt value
   */
  byte[] getSalt();

  /**
   * Java Secure Hashing with PBKDF2WithHmacSHA1
   *
   * @param authenticationInformation authentication information to be hashed
   * @param salt                      cryptographic salt
   * @param iterations                iterative number of encryption to be done
   *
   * @return an HashedAuthentication containing the result of the hashing
   */
  HashedAuthentication hash(String authenticationInformation, byte[] salt, int iterations);

}
